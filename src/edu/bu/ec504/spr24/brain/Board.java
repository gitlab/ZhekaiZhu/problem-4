package edu.bu.ec504.spr24.brain;

import edu.bu.ec504.spr24.sameGameTris.CircleColor;
import edu.bu.ec504.spr24.sameGameTris.SameGameTris;
import java.util.LinkedList;
import java.util.Objects;

/**
 * An internal recording of a board set up.
 */
class Board {

  /**
   * A 2-d Linked list of colors on the board.
   */
  final LinkedList<LinkedList<CircleColor>> data;

  /**
   * The width and height of the board.
   */
  final private int width, height;

  // constructs a board of specified width and height
  Board(int width, int height) {
    this.width = width;
    this.height = height;

    // allocate the data structure for storing board contents
    data = new LinkedList<>();

    // set up the data structure
    for (int ii = 0; ii < width; ii++) {
      LinkedList<CircleColor> temp = new LinkedList<>();
      for (int jj = 0; jj < height; jj++) {temp.add(CircleColor.NONE);}
      data.add(temp);
    }
  }

  /**
   * default constructor
   */
  Board() {
    this(SameGameTris.getInstance().boardWidth(), SameGameTris.getInstance().boardHeight());
  }

  /**
   * copy constructor
   *
   * @param baseBoard the board to copy
   */
  Board(Board baseBoard) {
    // allocate memory for the new board
    this(baseBoard.width, baseBoard.height);

    // copy over all the specific items
    for (int xx = 0; xx < columns(); xx++) {
      for (int yy = 0; yy < rows(xx); yy++) {modify(xx, yy, baseBoard.getAt(xx, yy));}
    }
  }

  /**
   * @return the color of the yy'th item in the xx'th column, or CircleColor.NONE
   * if the cell is not in the <b>data</b> container
   */
  CircleColor getAt(int xx, int yy) {
    try {
      return data.get(xx).get(yy);
    } catch (IndexOutOfBoundsException e) {
      return CircleColor.NONE;
    }
  }

  /**
   * @return the xx'th column, or null if the xx'th column does not exist in the <b>data</b> container
   */
  private LinkedList<CircleColor> getAt(int xx) {
    try {
      return data.get(xx);
    } catch (IndexOutOfBoundsException e) {
      return null;
    }
  }

  /**
   * Changes yy'th item in xx'th column
   * %R:  The circle at this location must still be in the <b>data</b> field
   */
  void modify(int xx, int yy, CircleColor col) {
    data.get(xx).set(yy, col);
  }

  /**
   * Deletes yy'th item in xx'th column
   * %R:  The circle at this location must still be in the <b>data</b> field
   */
  private void delete(int xx, int yy) {
    data.get(xx).remove(yy);
  }

  /**
   * Deletes the column at (horizontal) board location xx
   * * %R:  The column at this location must still be in the <b>data</b> field
   */
  private void delete(int xx) {
    data.remove(xx);
  }

  // public accessors

  /**
   * Simulates a click on the yy'th cell of the xx'th column
   *
   * @return the number of cells deleted
   * @modifies Deletes cells in the same region as cell (xx,yy)
   * @expects The clicked cell is assumed not to be empty
   */
  int clickNode(int xx, int yy) {
    CircleColor myColor = getAt(xx, yy); // the color to match

    // mark the region
    int count = clickNodeHelper(xx, yy, myColor);

    // clean up
    // ... delete all cells with no color
    // ... delete from the end to the front so that there are no problems with re-indexing
    for (int ii = data.size() - 1; ii >= 0; ii--) {
      for (int jj = Objects.requireNonNull(getAt(ii)).size() - 1; jj >= 0; jj--) {
        if (getAt(ii, jj) == CircleColor.NONE) {delete(ii, jj);}
      }

      // ... delete the column if it is empty
      if (Objects.requireNonNull(getAt(ii)).isEmpty()) {delete(ii);}
    }

    return count;
  }

  /**
   * Recursive procedure for propagating a click at a location with color "col".
   * All items in the same region as the clicked cell are made to have CircleColor.NONE
   *
   * @return the number of cells changed to CircleColor.NONE in this call (and its recursive subcalls)
   * @modifies the color of some cells, but no cells are actually deleted
   */
  int clickNodeHelper(int xx, int yy, CircleColor col) {
    if (getAt(xx, yy) == CircleColor.NONE ||
        // we've either already seen this, or we've hit an empty space
        getAt(xx, yy) != col)            // this is not the color we want
    {return 0;}

    // modify the current cell
    modify(xx, yy, CircleColor.NONE);

    return 1 + // 1 is for the current cell
        clickNodeHelper(xx - 1, yy, col) + // cell to the left
        clickNodeHelper(xx + 1, yy, col) + // cell to the right
        clickNodeHelper(xx, yy - 1, col) + // cell below
        clickNodeHelper(xx, yy + 1, col) // cell above
        ;
  }

  /**
   * @return the number of columns in the current state
   */
  int columns() {
    return data.size();
  }

  /**
   * @param xx the column being considered (assumed to exist)
   * @return the number of rows in column <b>xx</b>
   */
  int rows(int xx) {
    return data.get(xx).size();
  }


  /**
   * @return a "pretty-printed" version of the data structure
   */
  @Override
  public String toString() {
    String temp = data.toString();
    return temp.replace("], [", "]\n[");
  }

  /**
   * Stores an (xx,yy) position on the board.
   */
  static class Pos {
    final int xx;
    final int yy;

    Pos(int xx, int yy) {
      this.xx = xx;
      this.yy = yy;
    }

    @Override
    public String toString() {
      return "[" + xx + ", " + yy +']';
    }
  }
}
