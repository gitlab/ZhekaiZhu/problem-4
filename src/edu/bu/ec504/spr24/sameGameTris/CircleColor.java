package edu.bu.ec504.spr24.sameGameTris;

import java.awt.Color;

import static edu.bu.ec504.spr24.sameGameTris.SameGameTris.randGen;

/*
 * Records the color of one circle
 */

public enum CircleColor {
	NONE(null),
	Red(new Color(255, 0, 0)),
	Green(new Color(0, 255, 0)),
	Blue(new Color(0, 0, 255)),
	Pink(new Color(255, 0, 255));

	// FIELDS
	private final Color _myColor;

	// CONSTRUCTOR
	CircleColor(Color myColor) {
		this._myColor = myColor;
	}

	// ACCESSOR
	Color getColor() {
		return _myColor;
	}

	/*
	 * Generate a random color from the first num possible colors (excluding NONE)
	 */
	static CircleColor randColor() {
		if (SameGameTris.NUM_COLORS < 1 || SameGameTris.NUM_COLORS >= CircleColor.values().length)
			throw new IndexOutOfBoundsException("Only " + CircleColor.values().length
					+ " colors are available.  You requested choosing one of " + SameGameTris.NUM_COLORS + " colors.");

		int randNum = 1 + Math.abs(randGen.nextInt()) % SameGameTris.NUM_COLORS;
		return CircleColor.values()[randNum];
	}
}
